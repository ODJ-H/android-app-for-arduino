package com.example.rober.crashemergencylist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class EmergencyCrashList extends AppCompatActivity {

    private static final String TAG = "EmergencyCrashList";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_crash_list);
        ListView list = (ListView) findViewById(R.id.MainList);
        Log.d(TAG,"onCreate: Started.");

        String[] names = {"Emergency Contacts"};
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(adapter);
        

    }
}